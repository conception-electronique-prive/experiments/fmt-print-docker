#include <fmt/core.h>

int main() {
    std::size_t count = 0;
    while(true) {
        fmt::print("fmt {}\r\n", count++);
        std::fflush(nullptr);
    }
    return 0;
}
