#FROM ubuntu:latest
#RUN apt update
#RUN apt -y install gcc g++ make cmake git

FROM alpine:latest
RUN apk update
RUN apk add gcc g++ make cmake git

COPY . /app
WORKDIR /app
RUN cmake -S . -B build
WORKDIR /app/build
RUN cmake --build . -- -j ${nproc}
ENTRYPOINT ["./print_docker"]
